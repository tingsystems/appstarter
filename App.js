import React from 'react';
import PreLoader from "./application/components/PreLoader";

import { Ionicons } from '@expo/vector-icons';
import { observer } from 'mobx-react/native'
import {Provider} from 'mobx-react';
import { StyleProvider } from 'native-base';

import getTheme from './native-base-theme/components';
import variables from './native-base-theme/variables/material';
import store from './application/stores'

import GuestNavigation from './application/navigations/guest';

console.disableYellowBox = true;

@observer
export default class App extends React.Component {
	constructor () {
		super();
		
		this.state = {
			loaded: true
		}
	}

	async componentDidMount() {
		this.setState({ isReady: true });
	}

	render() {
		const { loaded } = this.state;
		if (!loaded) {
			return (<PreLoader/>);
		}
		return (
			<StyleProvider style={getTheme(variables)}>
				<Provider store={store}>
					<GuestNavigation />
				</Provider>
			</StyleProvider>
		);
	}
}
