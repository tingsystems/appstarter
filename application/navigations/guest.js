import React from 'react';
import { Platform } from 'react-native';
import { createBottomTabNavigator, createStackNavigator, createAppContainer } from "react-navigation";
import TabBarIcon from '../components/TabBarIcon';
import StartScreen from "../screens/Start";

const navigationOptions = {
	headerStyle: {
		backgroundColor: '#1f346a',
		color: '#fff'
	},
	headerTintColor: 'white',
	headerTitleStyle: {
		textAlign: 'center',
		alignSelf: 'center',
		fontSize: 20,
		color: '#fff',
		fontWeight: 'bold',
	}
}

const HomeStack = createStackNavigator({
	HomeScreen: {
		screen: StartScreen,
		navigationOptions: ({ navigation }) => ({
			title: 'Departamentos',
			...navigationOptions
		})
	}
});

HomeStack.navigationOptions = {
	tabBarLabel: 'Departametos',
	tabBarIcon: ({ focused }) => (
		<TabBarIcon
			focused={focused}
			name={
				Platform.OS === 'ios' ? 'ios-paper' : 'ios-paper'
			}
		/>
	),
};

const TabNavigator = createBottomTabNavigator(
	{
		Start: HomeStack,
	},
	{
		tabBarOptions: {
			activeTintColor: '#1f346a', 
			inactiveTintColor: 'gray',
		}
	});

export default createAppContainer(TabNavigator);