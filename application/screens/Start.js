import React, { Component } from 'react';
import { FlatList } from 'react-native-gesture-handler';

import { Content, Container, Text, List, ListItem } from 'native-base';
import Axios from 'axios';

export default class StartScreen extends Component {

	constructor() {
		super();
		this.state = {
			working: false,
			departments: [],
		}
	}

	componentDidMount() {
		this.getDepartments();
	}

	getDepartments() {
		Axios.get(
			'http://172.20.10.2:8000/departments/',
			{headers: {Accept: 'application/json'}}
		).then( (response) => {
			this.setState({ departments: response.data.results});
		}).catch( (error) => {
			console.log(error);
		})
	}

	renderDepartment(department) {
		return(
			<ListItem>
				<Text>{department.item.name}</Text>
			</ListItem>
		)
	}


	render() {
		const {departments} = this.state;
		return (
			<Container>
				<Content>
					<List>
						<FlatList
							data={departments}
							renderItem={ (department) => this.renderDepartment(department)}
							keyExtractor={ (department) => department.id.toString()}
						/>
					</List>
				</Content>
			</Container>
		);
	}
}